#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include "libnet.h"

void usage() {
	printf("syntax: pcap-test <interface>\n");
	printf("sample: pcap-test wlan0\n");
}

typedef struct {
	char* dev_;
} Param;

Param param = {
	.dev_ = NULL
};

bool parse(Param* param, int argc, char* argv[]) {
	if (argc != 2) {
		usage();
		return false;
	}
	param->dev_ = argv[1];
	return true;
}

int main(int argc, char* argv[]) {
	if (!parse(&param, argc, argv))
		return -1;

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
		return -1;
	}

	while (true) {
		struct pcap_pkthdr* header;
		const u_char* packet;
		int res = pcap_next_ex(pcap, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
			break;
		}
		printf("\n===============================\n");
		printf("%u bytes captured\n", header->caplen);

		struct libnet_ethernet_hdr* eth;
		struct libnet_ipv4_hdr* ip;
		struct libnet_tcp_hdr* tcp;

		eth = (struct libnet_ethernet_hdr*)packet;
		if (ntohs(eth->ether_type) != 0x0800) continue;
		ip = (struct libnet_ipv4_hdr*)(packet+sizeof(*eth));
		tcp = (struct libnet_tcp_hdr*)(packet+sizeof(*eth)+sizeof(*ip));
		if (ip->ip_p != 6) continue;

		printf("\nETHERNET\n");
		printf("MAC src : %02x:%02x:%02x:%02x:%02x:%02x\n", eth->ether_shost[0], eth->ether_shost[1], eth->ether_shost[2], eth->ether_shost[3], eth->ether_shost[4], eth->ether_shost[5]);
		printf("MAC dst : %02x:%02x:%02x:%02x:%02x:%02x\n", eth->ether_dhost[0], eth->ether_dhost[1], eth->ether_dhost[2], eth->ether_dhost[3], eth->ether_dhost[4], eth->ether_dhost[5]);

		printf("\nIP\n");
		u_int32_t IP_src = ip -> ip_src.s_addr;
		u_int32_t IP_dst = ip -> ip_dst.s_addr;
		u_int8_t s0, s1, s2, s3;

		s0 = (IP_src & 0xFF000000) >> 24;
		s1 = (IP_src & 0x00FF0000) >> 16;
		s2 = (IP_src & 0x0000FF00) >> 8;
		s3 = (IP_src & 0x000000FF);
		printf("IP src: %d.%d.%d.%d\n", s3, s2, s1, s0);

		s0 = (IP_dst & 0xFF000000) >> 24;
		s1 = (IP_dst & 0x00FF0000) >> 16;
		s2 = (IP_dst & 0x0000FF00) >> 8;
		s3 = (IP_dst & 0x000000FF);
		printf("IP dst: %d.%d.%d.%d\n", s3, s2, s1, s0);

		printf("\nTCP\n");
		printf("Port src: %d\n", ntohs(tcp -> th_sport));
		printf("Port dst: %d\n", ntohs(tcp -> th_dport));

		printf("\nDATA\n");
		u_int32_t payload = sizeof(*eth) + ip -> ip_hl * 4 + tcp->th_off * 4;
		u_int32_t payload_len = header -> caplen - payload;

		if (payload_len == 0){
			printf("NO DATA\n");
			continue;
		}
		for (int i = 0; i < payload_len; i++){
			if (i == 10) break;
			printf("%02x ",packet[payload + i]);
		}


	}

	pcap_close(pcap);
}

